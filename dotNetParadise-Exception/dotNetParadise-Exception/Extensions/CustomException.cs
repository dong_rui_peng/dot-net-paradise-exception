﻿namespace dotNetParadise_Exception.Extensions;

public class CustomException(int code, string message) : Exception(message)
{
    public int Code { get; private set; } = code;

    public string Message { get; private set; } = message;
}
